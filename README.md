# style-parser README
This extension helps converting css style text attributes to javascript notation. This is helpful for example when converting svg to a React element.

## Usage

Select a text to convert.
Run command "Convert CSS Styles to javascript" (press Ctrl+Shift+P)

## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.

## Release Notes

Users appreciate release notes as you update your extension.

### 1.0.0

First release

