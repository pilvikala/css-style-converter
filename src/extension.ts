// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { convertStyleAttributes } from './parser';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('style-parser.convert', () => {
		if(!vscode.window.activeTextEditor) {return;}
		const editor = vscode.window.activeTextEditor;
		if(!editor.selection.isEmpty) {
			const selectedText = editor.document.getText(new vscode.Range(editor.selection.start,editor.selection.end));
			const converted = convertStyleAttributes(selectedText);
			if(!converted) {return;}
			editor.edit((b)=>{
				b.replace(editor.selection, converted);
			});
			return;
		}
		const textToParse = editor.document.getText();
		const converted = convertStyleAttributes(textToParse);
		if(!converted) {return;}

		editor.edit((b)=>{
			b.delete(new vscode.Range(0,0,editor.document.lineCount, editor.document.lineAt(editor.document.lineCount).text.length));
			b.insert(new vscode.Position(0,0), converted);
		});
	});

	context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}
