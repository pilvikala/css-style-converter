import { convertStyleAttributes } from "./parser";

describe("paser", () => {

  const svgFragmentShort = '<path style="stroke: rgb(0, 0, 0)"></path>';
  const svgFragmentMalformed = '<path style="stroke: rgb(0, 0, 0)></path>';
  const svgFragmentNoStyles = '<path ></path>';
  const svgFragmentStyleWithMoreAttributes = `<path style="stroke: rgb(0, 0, 0); fill: none; color: black"></path>`;
  const svgFragmentStyleExtraSemicolons = `<path style="stroke: rgb(0, 0, 0);; fill: none; color: black;"></path>`;

  const svgFragmentMultiwordAttributes = `<path style="fill: none; stroke-width: 33px; stroke-linecap: round; totally-made-up: true"></path>`;

  const svgFragmentMultiplied = `<path style="stroke: rgb(0, 0, 0); fill: none; stroke-linejoin: round; stroke-linecap: round; stroke-width: 17px;"></path>
  <path style="stroke: rgb(0, 0, 0); fill: none; stroke-linecap: round; stroke-linejoin: round; stroke-width: 10px;"></path>
  <path style="fill: rgb(216, 216, 216); stroke: rgb(0, 0, 0); stroke-width: 33px; stroke-linecap: round; stroke-linejoin: round;"></path>`;

  test("replaces style attribute with object", () => {
    const result = convertStyleAttributes(svgFragmentShort);
    expect(result).toEqual(`<path style={{stroke: "rgb(0, 0, 0)"}}></path>`);
  });

  test("returns null if there's nothing to replace", () => {
    const result = convertStyleAttributes(svgFragmentNoStyles);
    expect(result).toEqual(null);
  });

  test("does not crash on malformed styles", () => {
    const result = convertStyleAttributes(svgFragmentMalformed);
    expect(result).toEqual(null);
  });

  test("replaces style attribute with object when there are more css attributes", () => {
    const result = convertStyleAttributes(svgFragmentStyleWithMoreAttributes);
    expect(result).toEqual(`<path style={{stroke: "rgb(0, 0, 0)", fill: "none", color: "black"}}></path>`);
  });

  test("ignores extra semicolons", () => {
    const result = convertStyleAttributes(svgFragmentStyleExtraSemicolons);
    expect(result).toEqual(`<path style={{stroke: "rgb(0, 0, 0)", fill: "none", color: "black"}}></path>`);
  });

  test("updates multi-word attributes", () => {
    const result = convertStyleAttributes(svgFragmentMultiwordAttributes);
    expect(result).toEqual(`<path style={{fill: "none", strokeWidth: "33px", strokeLinecap: "round", totallyMadeUp: "true"}}></path>`);
  });

  test("converts multiple style attributes", () => {
    const result = convertStyleAttributes(svgFragmentMultiplied);
    expect(result).toEqual(`<path style={{stroke: "rgb(0, 0, 0)", fill: "none", strokeLinejoin: "round", strokeLinecap: "round", strokeWidth: "17px"}}></path>
  <path style={{stroke: "rgb(0, 0, 0)", fill: "none", strokeLinecap: "round", strokeLinejoin: "round", strokeWidth: "10px"}}></path>
  <path style={{fill: "rgb(216, 216, 216)", stroke: "rgb(0, 0, 0)", strokeWidth: "33px", strokeLinecap: "round", strokeLinejoin: "round"}}></path>`);
  });
});
