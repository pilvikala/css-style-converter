interface StyleAttribute {
  start: number;
  end: number;
  content: string;
}

const getStyleAttribute = (text: string, startIndex: number): StyleAttribute | null=> {
  const start = text.indexOf('style="', startIndex);
  if(start === -1) {
    return null;
  }
  const end = text.indexOf('"', start + 7);
  if(end === -1) {
    return null;
  }
  return {start, end, content: text.substring(start + 7, end)};
};


const getStyleAttributes = (text: string): StyleAttribute[] => {
  const styleAttributes: StyleAttribute[] = [];
  let startIndex = 0;
  do {
    const attr = getStyleAttribute(text, startIndex);
    if(!attr) {return styleAttributes;}
    styleAttributes.push(attr);
    startIndex = attr.end;
  } while(startIndex < text.length);
  return styleAttributes;
};

const capitalize = (text: string): string => {
  return text.charAt(0).toUpperCase()
  + text.slice(1);
};

const fixMultiwordAttributeName = (name: string): string => {
  const hyphenIndex = name.indexOf("-");
  if(hyphenIndex === -1) {return name;}
  const parts = name.split("-");
  return parts.map((p, i)=> i !== 0 ? capitalize(p) : p).join("");
};

const convertValuePair = (cssValue: string): string => {
  const cssAttributeValuePair = cssValue.split(":").map((a)=>a.trim());
  const replaceCssValue = `${fixMultiwordAttributeName(cssAttributeValuePair[0])}: "${cssAttributeValuePair[1]}"`;
  return replaceCssValue;
};


const convertStyleAttribute = (styleAttribute: StyleAttribute, sourceText: string): string => {
  const cssValuePairs = styleAttribute.content.split(";").filter((p)=>Boolean(p)).map(convertValuePair);
  const replaceStyleAttribute = cssValuePairs.join(", ");
  let convertedText = sourceText;
  convertedText = sourceText.replace(`"${styleAttribute.content}"`, `{{${replaceStyleAttribute}}}`);
  return convertedText;
};

export const convertStyleAttributes = (text: string): string | null => {
  const styleAttributes = getStyleAttributes(text);
  if(styleAttributes.length === 0) {return null;}
  let result = text;
  styleAttributes.forEach((attr)=>{
    result = convertStyleAttribute(attr, result);
  });
  return result;
};

